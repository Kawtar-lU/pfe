import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class TestPfe {

    // Functions
    public static void maximize(WebDriver driver) {
        driver.manage().window().maximize();
    }

    public static void setUrl(WebDriver driver, String url) {
        driver.get(url);
    }
    public static void title(WebDriver driver) {

        String t = driver.getTitle();
        if (t.isEmpty() == true)
            System.out.println("Title not found !");
        else
            System.out.println("Title : " + t);
    }
    public static void pageSource(WebDriver driver) {

        String p = driver.getPageSource();
        System.out.println("Page Source is : " + p);

    }

    public static void verifyBrand(WebDriver driver, String brand) {

        WebElement element = driver.findElement(By.className("navbar-brand"));
        String text = element.getText();

        if (text.equals(brand))
            System.out.println("Correct Brand");
        else
            System.out.println("Incorrect Brand");

    }

    public static void byTagName(WebDriver driver, String tag) {
        String text = driver.findElement(By.tagName(tag)).getText();
        System.out.println(tag + " : " + text);
    }

    public static void byClassName(WebDriver driver, String c) {
        String text = driver.findElement(By.className(c)).getText();
        System.out.println(c + " : " + text);
    }

    public static void byId(WebDriver driver, String ident) {
        String text = driver.findElement(By.id(ident)).getText();
        System.out.println(ident + " : " + text);
    }
    public static void click(WebDriver driver, String x) {
        WebElement element = driver.findElement(By.className(x));
        element.click();
    }
    WebDriver driver;
    @BeforeTest
    public void beforeTest() {
        // browser type and chromedriver.exe path as parameters
        System.setProperty("webdriver.chrome.driver", "C:\\tools\\chromedriver.exe");
        driver = new ChromeDriver();
    }
    @Test
    public void test() {
        System.out.println("/------------------------------------------------------------------------/");
        maximize(driver);
        setUrl(driver,"file:///C:/Users/hp/OneDrive/Bureau/Usecase-1/learning-sample/index.html");
        title(driver);
        verifyBrand(driver, "Docker Sample");
        byTagName(driver,"h1");
        byTagName(driver,"footer");
        click(driver,"btn");
        System.out.println("/------------------------------------------------------------------------/");
    }
    @AfterTest
    public void afterTest() {
        driver.close();
    }
}

